import pandas
import numpy
# import sklearn
import matplotlib

train = pandas.read_csv('./train.csv')
print(train)
nb_survivant = train.query(expr= "Survived == 1")
print("survivants: ", len(nb_survivant))
nb_mort = train.query(expr= "Survived == 0")
print("mort: ", len(nb_mort))
nb_null = len(train.isna().query(expr= "Survived == True"))
print("null: ", nb_null)
age_moyen = train['Age'].mean()
print("age moyen: ", age_moyen)
print(train['Age'].head(10))
train.Age.fillna(age_moyen, inplace=True)
print(train["Age"].head(10))
# for index, row in train.iterrows():
#     print(row)